package com.testApp.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.datanucleus.store.types.sco.backed.Map;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;













import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;
import com.google.gson.Gson;
//import com.google.appengine.repackaged.com.google.gson.Gson;
import com.testApp.PMF;
import com.testApp.model.Customer;
import com.testApp.model.UsersData;

@Controller
@RequestMapping("/users")
public class CustomerController {

	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public void LandingPage(ModelMap model,HttpServletRequest req, HttpServletResponse resp) throws IOException {

		 System.out.println("All the datas are sending from backend to front end.... running");
		 String retVal ="";
			HashMap<String,Object> response = new HashMap<String,Object>();
			response.put("status", false);
			PersistenceManager pm= PMF.get().getPersistenceManager();
			
				Query q = pm.newQuery(UsersData.class);

				
				
				//q.setOrdering("date desc");
				List<UsersData> list= (List<UsersData>) q.execute();
				if(list.size()>0)
				{
				Gson obj = new Gson();
				retVal = obj.toJson(list);
				response.put("status", true);
				response.put("list", retVal);
				}
				resp.getWriter().write(retVal);
				//return retVal;

	}
	
	@RequestMapping(value = "", method = RequestMethod.POST)
	public void SavingData(@RequestBody String body, ModelMap model,HttpServletRequest req, HttpServletResponse resp) throws IOException {

		 System.out.println("Saving new User.... running");
		 
		 System.out.println(body);
		 
			
			PersistenceManager pm= PMF.get().getPersistenceManager();
		    JSONObject JSON = null;
			try {
				JSON = new JSONObject(body);
				String firstname=JSON.getString("firstname");
				String lastname=JSON.getString("lastname");
				String age=JSON.getString("age");
				
				
				
				// Persisting new Phone details
				UsersData r=new UsersData();
				r.setFirstname(firstname);
				r.setLastname(lastname);
				r.setAge(age);
				
		        pm.makePersistent(r);
				
		        String s = "Registration Success.";
				System.out.println(s);
				resp.getWriter().write(body);
				
			} catch (JSONException e) {
				
				e.printStackTrace();
			}
			finally {
				pm.close();
			}
		 

	}
	

	@RequestMapping(method=RequestMethod.GET, value="/{id}")
    @ResponseBody
	 public String editing(HttpServletRequest request,HttpServletResponse resp, ModelMap model,@PathVariable String id) throws IOException {
    		PersistenceManager pm= PMF.get().getPersistenceManager();
    		
    		
    		System.out.println("Update Get is calling");
    		long key = Long.parseLong(id);
			Query q = pm.newQuery(UsersData.class);
			q.setFilter("id == idParameter");
			//q.setFilter("name == nameParameter");
			//q.setFilter("id == idParameter"+"&&"+"id == '"+id+"'");
			q.declareParameters("String idParameter");
			List<UsersData> list= (List<UsersData>) q.execute(key);
			UsersData	UDobj	=	(UsersData)list.get(0);
			
			Gson obj = new Gson();
			String retVal = obj.toJson(UDobj);
			
			return retVal;

		}
    @RequestMapping(method=RequestMethod.PUT, value="/{id}")
	public void updateEntry(@RequestBody String data, HttpServletRequest request, HttpServletResponse resp,@PathVariable String id) throws IOException{
		
		String k=id;
		long key=Long.parseLong(k);
		PersistenceManager pm= PMF.get().getPersistenceManager();
		
		System.out.println("PUT IS CAlling for updation");
			
			
		    
		    
		    //Key key=KeyFactory.stringToKey(key1);
		    JSONObject JSON = null;
			try {
				JSON = new JSONObject(data);
				String firstname=JSON.getString("firstname");
				String lastname=JSON.getString("lastname");
				String age=JSON.getString("age");

				
				UsersData US = pm.getObjectById(UsersData.class, key);
				US.setFirstname(firstname);
				US.setLastname(lastname);
				US.setAge(age);
				
				resp.getWriter().write(data);


			
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
    
    @RequestMapping(method=RequestMethod.DELETE, value="/{id}")
    public void deleteData(HttpServletRequest request,HttpServletResponse resp, @PathVariable String id) throws IOException {
    	
    	System.out.println("Delete is calling");
    	
    	
    	String k=id;
		long key=Long.parseLong(k);
    	
    	try{
    		
	    	PersistenceManager pm = PMF.get().getPersistenceManager();
	    	

    		UsersData c = pm.getObjectById(UsersData.class, key);
    		pm.deletePersistent(c);
    	}
    	catch(Exception e){
    		System.out.println(e);
    	}
    	resp.getWriter().write(k);
    }
			

    }
    

